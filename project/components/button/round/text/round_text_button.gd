extends StaticBody


signal switch_mode(mode)


export (String) var button_text
export (bool) var active = false
export (String, "x", "y", "z") var translation_axis = "x"
export (bool) var negative_translation = false

const ID = "button"
const MOVE_SPEED = 1.5
const INACTIVE_COLOR = Color("#3a3a2e")
const OFF_COLOR = Color("#b0b773")
const ON_COLOR = Color("#44a535")

enum MODE {OFF, ON}

var move_distance
var min_height
var max_height
var current_mode


func _ready():
	var own_material = SpatialMaterial.new()
	own_material.set_albedo(INACTIVE_COLOR)
	$BodyMesh.set_surface_material(0, own_material)
	
	max_height = get_translation()[translation_axis]
	move_distance = 0.7 * scale[translation_axis]
	if active:
		_set_surface_color(OFF_COLOR)
	if negative_translation:
		min_height = max_height + move_distance
	else:
		min_height = max_height - move_distance
	current_mode = MODE.OFF
	$Viewport/Control/Label.text = button_text


func activate():
	active = true
	if current_mode == MODE.ON:
		_set_surface_color(ON_COLOR)
	elif current_mode == MODE.OFF:
		_set_surface_color(OFF_COLOR)


func deactivate():
	active = false
	_set_surface_color(INACTIVE_COLOR)


func do_action():
	if active:
		if current_mode == MODE.OFF:
			current_mode = MODE.ON
			_set_surface_color(ON_COLOR)
		elif current_mode == MODE.ON:
			current_mode = MODE.OFF
			_set_surface_color(OFF_COLOR)
		emit_signal("switch_mode", current_mode)


func _physics_process(delta):
	if current_mode == MODE.ON:
		if get_translation()[translation_axis] > min_height:
			translate_object_local(Vector3(0, -MOVE_SPEED * delta, 0))
			if get_translation()[translation_axis] < min_height:
				_set_own_translation(min_height)
		elif get_translation()[translation_axis] < min_height:
			translate_object_local(Vector3(0, MOVE_SPEED * delta, 0))
			if get_translation()[translation_axis] > min_height:
				_set_own_translation(min_height)
	elif current_mode == MODE.OFF:
		if get_translation()[translation_axis] < max_height:
			translate(Vector3(0, MOVE_SPEED * delta, 0))
			if get_translation()[translation_axis] > max_height:
				_set_own_translation(max_height)
		elif get_translation()[translation_axis] > max_height:
			translate_object_local(Vector3(0, -MOVE_SPEED * delta, 0))
			if get_translation()[translation_axis] < max_height:
				_set_own_translation(max_height)


func _set_surface_color(new_color):
	var current_material = $BodyMesh.get_surface_material(0)
	if current_material is SpatialMaterial:
		current_material.set_albedo(new_color)
#		$BodyMesh.set_surface_material(0, current_material)


func _set_own_translation(new_position):
	var current_translation = get_translation()
	current_translation[translation_axis] = new_position
	set_translation(current_translation)
