extends Spatial


signal switch_mode(mode)


const OFF_COLOR = Color("#b01120")
const ON_COLOR = Color("#22b022")
const OFF_HEIGHT = 1.9
const ON_HEIGHT = 1
const MOVE_SPEED = 0.05

enum MODE {OFF, ON}

var mode
var on_material = SpatialMaterial.new()
var off_material = SpatialMaterial.new()


# Called when the node enters the scene tree for the first time.
func _ready():
	mode = MODE.OFF
	on_material.set_albedo(ON_COLOR)
	off_material.set_albedo(OFF_COLOR)


func _physics_process(_delta):
	if mode == MODE.ON:
		if $ButtonHead.translation.y > ON_HEIGHT:
			$ButtonHead.translation.y -= MOVE_SPEED
			if $ButtonHead.translation.y < ON_HEIGHT:
				$ButtonHead.translation.y = ON_HEIGHT
	elif mode == MODE.OFF:
		if $ButtonHead.translation.y < OFF_HEIGHT:
			$ButtonHead.translation.y += MOVE_SPEED
			if $ButtonHead.translation.y > OFF_HEIGHT:
				$ButtonHead.translation.y = OFF_HEIGHT
	
	if mode == MODE.ON:
		$ButtonHead/MeshInstance.set_surface_material(0, on_material)
	else:
		$ButtonHead/MeshInstance.set_surface_material(0, off_material)


func on_press():
	if mode == MODE.OFF:
		mode = MODE.ON
	else:
		mode = MODE.OFF
	emit_signal("switch_mode", mode)
