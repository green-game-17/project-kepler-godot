extends Spatial


var unlock_counter = 0
var current_unlock = 0
var blocked = false


# Called when the node enters the scene tree for the first time.
func _ready():
	_unlock(1)


func start_unlock():
	$Timer.start()


func _unlock(level):
	var root = get_node_or_null("L" + str(level) + "Root")
	if root:
		root.get_node("L" + str(level)).activate()
		root.get_node("E1").activate()
		root.get_node("E2").activate()
		root.get_node("E3").activate()
		current_unlock = level


func _on_Timer_timeout():
	unlock_counter += 1
	var pb = get_node_or_null("L" + str(current_unlock) + "-L" + str(current_unlock + 1))
	if pb:
		pb.set_percentage(unlock_counter)
	if unlock_counter == 100:
		unlock_counter = 0
		$Timer.stop()
		_unlock(current_unlock + 1)
		blocked = false


func _on_E3_switch_mode(mode):
	if mode == 1 && !blocked:
		start_unlock()
		blocked = true
