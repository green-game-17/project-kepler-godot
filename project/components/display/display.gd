extends Spatial


export (int) var viewport_width = 1000
export (int) var viewport_height = 500


# Called when the node enters the scene tree for the first time.
func _ready():
	$Viewport.set_size(Vector2(viewport_width, viewport_height))


func _set_text(text):
	$Viewport/Control/Panel/Label.set_text(text)


func _on_Button_switch_mode(mode):
	if mode == 0:
		_set_text("Der Knopf ist nicht gedrückt!")
	elif mode == 1:
		_set_text("Der Knopf ist gedrückt!")
