extends StaticBody


const MAX_HEIGHT = 1.6
const MIN_HEIGHT = 0
const HEIGHT_SPEED = 0.05

var current_height = 1.6
var dest_height = 1.6
var y_pos = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _physics_process(_delta):
	if current_height != dest_height:
		if current_height < dest_height:
			current_height += HEIGHT_SPEED
			if current_height > MAX_HEIGHT:
				current_height = MAX_HEIGHT
		elif current_height > dest_height:
			current_height -= HEIGHT_SPEED
			if current_height < MIN_HEIGHT:
				current_height = MIN_HEIGHT
		$Progress.scale.y = current_height
		$Progress.translation.y = -(MAX_HEIGHT / 2) + current_height / 2


func _on_Button_switch_mode(mode):
	if mode == 1:
		dest_height = MIN_HEIGHT
	elif mode == 0:
		dest_height = MAX_HEIGHT
