extends Spatial


export (SpatialMaterial) var bar_material
export (int) var start_percentage
export (String) var key

const MOVE_SPEED = 0.05
const MIN_X = -1.001
const MAX_X = 0

var goal_percentage = 0


func _ready():
	if bar_material:
		$BarMesh.set_surface_material(0, bar_material)
	if start_percentage:
		var current_translation = $BarMesh.get_translation()
		current_translation.x = MIN_X + start_percentage * ((MAX_X - MIN_X) / 100)
		$BarMesh.set_translation(current_translation)
		goal_percentage = start_percentage


func set_percentage(new_percentage: int):
	goal_percentage = new_percentage


func _physics_process(delta):
	var current_translation = $BarMesh.get_translation()
	if goal_percentage < _get_percentage(current_translation.x):
		current_translation.x -= MOVE_SPEED * delta
		if goal_percentage > _get_percentage(current_translation.x):
			current_translation.x = MIN_X + goal_percentage * ((MAX_X - MIN_X) / 100)
	elif goal_percentage > _get_percentage(current_translation.x):
		current_translation.x += MOVE_SPEED * delta
		if goal_percentage < _get_percentage(current_translation.x):
			current_translation.x = MIN_X + goal_percentage * ((MAX_X - MIN_X) / 100)
	$BarMesh.set_translation(current_translation)
	$BarMesh.set_scale(Vector3(1.002 + current_translation.x, 1, 1))


func _get_percentage(x_pos) -> float:
	return (x_pos - MIN_X) / ((MAX_X - MIN_X) / 100)


func _on_new_percentage(percentages):
	set_percentage(round(percentages[key]) as int)
