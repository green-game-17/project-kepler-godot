extends PanelContainer


const NAME_LIST = [
	"Peter Lustig",
	"Yamete Oniichan",
	"Spongebob Squarepants",
	"The Flying Dutchman",
	"Astolfo",
	"Jeffrey Henchman",
	"Anonymous",
	"Harambe"
]
const TEXT_LIST = {
	"passengers": {
		"full": [
			"It's getting really packed, where are all these people coming from?",
			"Wow, they're really going at it, so many babys and no end in sight"
		],
		"empty": [
			"It's been so long since I saw someone, I wonder if there are enough people to continue this mission?",
			"So many people are choosing career over family, can we keep this mission going?"
		]
	},
	"food": {
		"low": [
			"Dang, our rations keep getting smaller and smaller",
			"I'm hungry..."
		]
	},
	"water": {
		"low": [
			"I never imagined I could be this thirsty",
			"Last time I showered was 3 months ago. But I should not waste the water"
		]
	},
	"air": {
		"low": [
			"Even though I just crawled out of my bed, I'm short of breath. Does this mean I need to do more sports?",
			"Feels like I'm on a mountain, the air is so thin"
		]
	},
	"satisfaction": {
		"low": [
			"What is the captain even doing? Everything seems to get worse",
			"I heard some people are really dissatisfied, I kind of understand their reasoning..."
		]
	},
	"disease_risk": {
		"high": [
			"I better avoid public transportation, the risk to get ill is too damn high",
			"People stay at home, diseases are going around way too quickly"
		]
	},
	"education": {
		"low": [
			"I just got asked what 2+2 equals? Is it just me, or are the people getting dumber?"
		],
		"high": [
			"Just set a new speedrun record with solving my homework in under 3 seconds! Feeling smart today"
		]
	},
	"daijoubu": {
		"good": [
			"It's a beautiful day today, keep up the good work!",
			"Nothing bad happened yesterday, I hope it stays this way"
		],
		"bad": [
			"I hate that nothing is happening, kinda boring ngl",
			"Again no news, I mean it's good, but...",
			"BOOOOOORING"
		]
	}
}


func _ready():
	randomize()


func set_texts(surv_stat_perc, pass_stat_perc):
	var surv_warnings = []
	var pass_warnings = []
	var tags = null
	var text_array = null
	
	for key in surv_stat_perc:
		if key == "passengers":
			if surv_stat_perc[key][0] > 100:
				surv_warnings.append([key, "full"])
			elif surv_stat_perc[key][1] <= 100:
				surv_warnings.append([key, "empty"])
		else:
			if surv_stat_perc[key] < 20:
				surv_warnings.append([key, "low"])
	
	if pass_stat_perc.satisfaction < 20:
		pass_warnings.append(["satisfaction", "low"])
	if pass_stat_perc.disease_risk > 60:
		pass_warnings.append(["disease_risk", "high"])
	if pass_stat_perc.education < 20:
		pass_warnings.append(["education", "low"])
	if pass_stat_perc.education > 80:
		pass_warnings.append(["education", "high"])
	
	if surv_warnings.size() > 0 && pass_warnings.size() > 0:
		if randi() % 5 != 0:
			tags = surv_warnings[randi() % surv_warnings.size()]
		else:
			tags = pass_warnings[randi() % pass_warnings.size()]
	elif surv_warnings.size() > 0:
		tags = surv_warnings[randi() % surv_warnings.size()]
	elif pass_warnings.size() > 0:
		tags = pass_warnings[randi() % pass_warnings.size()]
	else:
		if randi() % 2 == 0:
			tags = ["daijoubu", "good"]
		else:
			tags = ["daijoubu", "bad"]
	
	text_array = TEXT_LIST[tags[0]][tags[1]]
	
	$MarginContainer/VBoxContainer/NameLabel.set_text(NAME_LIST[randi() % NAME_LIST.size()])
	$MarginContainer/VBoxContainer/TextLabel.set_text(text_array[randi() % text_array.size()])


func _on_DeleteTimer_timeout():
	queue_free()
