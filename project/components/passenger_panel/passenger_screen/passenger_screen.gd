extends Spatial


export (PackedScene) var thought_scene

onready var thought_column_1 = $Viewport/Control/ThoughtHBox/ThoughtColumn1
onready var thought_column_2 = $Viewport/Control/ThoughtHBox/ThoughtColumn2
onready var stat_table = $Viewport/Control/StatisticsTable/MarginContainer/HBoxContainer


func _ready():
	randomize()


func _set_new_value(key, value):
	var progress_node = null
	match key:
		"passengers":
			progress_node = stat_table.get_node("ProgressBarVBox/PassengersProgress")
		"food":
			progress_node = stat_table.get_node("ProgressBarVBox/FoodProgress")
		"water":
			progress_node = stat_table.get_node("ProgressBarVBox/WaterProgress")
		"air":
			progress_node = stat_table.get_node("ProgressBarVBox/AirProgress")
	if progress_node:
		progress_node.set_value(value)
		progress_node.get_node("Label").set_text(_get_number_text(round(value)))


func _set_new_max_value(key, max_value):
	var label_node = null
	var progress_node = null
	match key:
		"passengers":
			label_node = stat_table.get_node("MaxValuesVBox/PassengersPanel/Label")
			progress_node = stat_table.get_node("ProgressBarVBox/PassengersProgress")
		"food":
			label_node = stat_table.get_node("MaxValuesVBox/FoodPanel/Label")
			progress_node = stat_table.get_node("ProgressBarVBox/FoodProgress")
		"water":
			label_node = stat_table.get_node("MaxValuesVBox/WaterPanel/Label")
			progress_node = stat_table.get_node("ProgressBarVBox/WaterProgress")
		"air":
			label_node = stat_table.get_node("MaxValuesVBox/AirPanel/Label")
			progress_node = stat_table.get_node("ProgressBarVBox/AirProgress")
	if label_node:
		label_node.set_text(_get_number_text(max_value))
	if progress_node:
		progress_node.set_max(max_value)


func generate_new_thought(surv_stat_perc, pass_stat_perc):
	var new_thought = thought_scene.instance()
	new_thought.set_texts(surv_stat_perc, pass_stat_perc)
	var col1_children = thought_column_1.get_children()
	var col2_children = thought_column_2.get_children()
	if col1_children.size() == 0:
		thought_column_1.add_child(new_thought)
	elif col2_children.size() == 0:
		thought_column_2.add_child(new_thought)
	else:
		var last_child1 = col1_children[-1]
		var y1 = last_child1.get_position().y + last_child1.get_size().y
		var last_child2 = col2_children[-1]
		var y2 = last_child2.get_position().y + last_child2.get_size().y
		if y1 <= y2:
			thought_column_1.add_child(new_thought)
		else:
			thought_column_2.add_child(new_thought)


func _get_number_text(number) -> String:
	var regex = RegEx.new()
	regex.compile("\\d(?=(\\d{3})+($|\\.))")
	return regex.sub(str(number), "$0.", true)


func _on_new_survival_value(key, survival_value, is_max):
	if is_max:
		_set_new_max_value(key, survival_value)
	else:
		_set_new_value(key, survival_value)
