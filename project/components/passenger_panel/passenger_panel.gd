extends Spatial


signal send_dict(dict)
signal new_percentage(percentages)
signal new_survival_value(key, survival_value, is_max)


const SHOP_DICT = {
	"grocery_store": {
		"name": "Grocery store",
		"reproduction_rate": 0,
		"satisfaction": 5,
		"disease_risk": 0,
		"community_spirit": 6,
		"education": 0
	},
	"pharmacy": {
		"name": "Pharmacy",
		"reproduction_rate": -1,
		"satisfaction": 0,
		"disease_risk": -5,
		"community_spirit": 0,
		"education": 0
	},
	"casino": {
		"name": "Casino",
		"reproduction_rate": -3,
		"satisfaction": 4,
		"disease_risk": 0,
		"community_spirit": -1,
		"education": 0
	},
	"swimming_pool": {
		"name": "Swimming pool",
		"reproduction_rate": 0,
		"satisfaction": 6,
		"disease_risk": 4,
		"community_spirit": 3,
		"education": 0
	},
	"gym": {
		"name": "Gym",
		"reproduction_rate": 4,
		"satisfaction": 4,
		"disease_risk": -2,
		"community_spirit": 2,
		"education": 0
	},
	"jewelry_store": {
		"name": "Jewelry store",
		"reproduction_rate": 2,
		"satisfaction": 2,
		"disease_risk": 0,
		"community_spirit": 1,
		"education": 0
	},
	"cinema": {
		"name": "Cinema",
		"reproduction_rate": 1,
		"satisfaction": 5,
		"disease_risk": 0,
		"community_spirit": 0,
		"education": 0
	},
	"red_light_district": {
		"name": "Red light district",
		"reproduction_rate": -5,
		"satisfaction": 5,
		"disease_risk": 3,
		"community_spirit": 0,
		"education": 0
	},
	"cultural_center": {
		"name": "Cultural center",
		"reproduction_rate": 0,
		"satisfaction": 0,
		"disease_risk": 0,
		"community_spirit": 2,
		"education": 2
	},
	"library": {
		"name": "Library",
		"reproduction_rate": 0,
		"satisfaction": 1,
		"disease_risk": 0,
		"community_spirit": 0,
		"education": 4
	},
	"church": {
		"name": "Church",
		"reproduction_rate": -2,
		"satisfaction": 2,
		"disease_risk": 0,
		"community_spirit": 6,
		"education": 0
	},
	"furniture_store": {
		"name": "Furniture store",
		"reproduction_rate": 0,
		"satisfaction": 3,
		"disease_risk": 0,
		"community_spirit": 0,
		"education": 0
	},
	"police_station": {
		"name": "Police station",
		"reproduction_rate": 0,
		"satisfaction": 2,
		"disease_risk": 0,
		"community_spirit": 3,
		"education": 0
	},
	"prison": {
		"name": "Prison",
		"reproduction_rate": 0,
		"satisfaction": 1,
		"disease_risk": 0,
		"community_spirit": 4,
		"education": 0
	},
	"delivery_service": {
		"name": "Delivery service",
		"reproduction_rate": 1,
		"satisfaction": 1,
		"disease_risk": 1,
		"community_spirit": -1,
		"education": 0
	},
	"electronics_store": {
		"name": "Electronics store",
		"reproduction_rate": 0,
		"satisfaction": 3,
		"disease_risk": 0,
		"community_spirit": -1,
		"education": 0
	},
	"restaurants": {
		"name": "Restaurants",
		"reproduction_rate": 2,
		"satisfaction": 0,
		"disease_risk": 0,
		"community_spirit": 0,
		"education": 0
	},
	"spa": {
		"name": "Spa",
		"reproduction_rate": 1,
		"satisfaction": 4,
		"disease_risk": 0,
		"community_spirit": 0,
		"education": 0
	},
	"clothing_store": {
		"name": "Clothing store",
		"reproduction_rate": 0,
		"satisfaction": 1,
		"disease_risk": 0,
		"community_spirit": 0,
		"education": 0
	},
	"public_transportation": {
		"name": "Public transportation",
		"reproduction_rate": 0,
		"satisfaction": 2,
		"disease_risk": 7,
		"community_spirit": 0,
		"education": 0
	},
	"park": {
		"name": "Park",
		"reproduction_rate": 0,
		"satisfaction": 3,
		"disease_risk": 0,
		"community_spirit": 2,
		"education": 0
	},
	"bar": {
		"name": "Bar",
		"reproduction_rate": 1,
		"satisfaction": 4,
		"disease_risk": 0,
		"community_spirit": 4,
		"education": 1
	},
	"disco": {
		"name": "Disco",
		"reproduction_rate": 1,
		"satisfaction": 3,
		"disease_risk": 0,
		"community_spirit": 3,
		"education": 0
	},
	"hospital": {
		"name": "Hospital",
		"reproduction_rate": 5,
		"satisfaction": 2,
		"disease_risk": -8,
		"community_spirit": 0,
		"education": 0
	},
	"educational_institutions": {
		"name": "Educational institutions",
		"reproduction_rate": 0,
		"satisfaction": -3,
		"disease_risk": 0,
		"community_spirit": 2,
		"education": 10
	},
	"cafe": {
		"name": "Cafe",
		"reproduction_rate": 0,
		"satisfaction": 2,
		"disease_risk": 0,
		"community_spirit": 2,
		"education": 1
	},
	"stadium": {
		"name": "Stadium",
		"reproduction_rate": 0,
		"satisfaction": 0,
		"disease_risk": 5,
		"community_spirit": 5,
		"education": 0
	},
	"amusement_park": {
		"name": "Amusement park",
		"reproduction_rate": 0,
		"satisfaction": 5,
		"disease_risk": 0,
		"community_spirit": 1,
		"education": 0
	}
}
const EVALUATION_SPEED = 0.05
const MAX_VALUES = {
	"reproduction_rate": 18,
	"satisfaction": 70,
	"disease_risk": 20,
	"community_spirit": 46,
	"education": 18
}
const DEATH_RATE = 0.0077
const DISEASE_MOTRALITY = 0.95
const PASSENGER_DAMPENING = 500

enum BUTTON_MODE {OFF, ON}

var pass_stat_perc = {
	"reproduction_rate": 50.0,
	"satisfaction": 50.0,
	"disease_risk": 50.0,
	"community_spirit": 50.0,
	"education": 50.0
}
var shop_values = {
	"reproduction_rate": 0,
	"satisfaction": 0,
	"disease_risk": 0,
	"community_spirit": 0,
	"education": 0
}
var max_surv_values = {
	"passengers": 250,
	"food": 1000,
	"water": 10000,
	"air": 500
}
var survival_values = {
	"passengers": 200.0,
	"food": 500.0,
	"water": 1000.0,
	"air": 300.0
}


# Called when the node enters the scene tree for the first time.
func _ready():
	emit_signal("send_dict", SHOP_DICT)
	for key in survival_values:
		emit_signal("new_survival_value", key, max_surv_values[key], true)
		emit_signal("new_survival_value", key, survival_values[key], false)


func _physics_process(delta):
	for key in pass_stat_perc:
		var perc = float(shop_values[key]) / (float(MAX_VALUES[key]) / 2.0)
		var speed = EVALUATION_SPEED * delta
		pass_stat_perc[key] += (perc - 1.0) * speed
		pass_stat_perc[key] = clamp(pass_stat_perc[key], 0.0, 100.0)
	emit_signal("new_percentage", pass_stat_perc)
	
	var new_passengers = survival_values.passengers / PASSENGER_DAMPENING * delta * (
		pass_stat_perc.reproduction_rate / 100
	)
	survival_values.passengers += new_passengers
	new_passengers = survival_values.passengers / PASSENGER_DAMPENING * delta * (
		DEATH_RATE + (pass_stat_perc.disease_risk / 100) * DISEASE_MOTRALITY
	)
	survival_values.passengers -= new_passengers
	emit_signal("new_survival_value", "passengers", survival_values.passengers, false)


func _on_button_press(key, mode):
	var values = SHOP_DICT[key]
	if mode == BUTTON_MODE.OFF:
		shop_values.reproduction_rate -= values.reproduction_rate
		shop_values.satisfaction -= values.satisfaction
		shop_values.disease_risk -= values.disease_risk
		shop_values.community_spirit -= values.community_spirit
		shop_values.education -= values.education
	else:
		shop_values.reproduction_rate += values.reproduction_rate
		shop_values.satisfaction += values.satisfaction
		shop_values.disease_risk += values.disease_risk
		shop_values.community_spirit += values.community_spirit
		shop_values.education += values.education


func _on_ThoughtTimer_timeout():
	var surv_stat_perc = {}
#	Passengers additionally give the total amount for correct fail condition
	for key in survival_values:
		if key == "passengers":
			surv_stat_perc[key] = [(survival_values[key] / max_surv_values[key]) * 100, survival_values[key]]
		else:
			surv_stat_perc[key] = (survival_values[key] / max_surv_values[key]) * 100
	$PassengerScreen.generate_new_thought(surv_stat_perc, pass_stat_perc)
