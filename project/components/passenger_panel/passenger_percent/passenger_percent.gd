extends Spatial


export (String) var key


func set_percentage(new_percentage: int):
	if new_percentage == 100:
		$Viewport/Control/Label.text = str(new_percentage) + '%'
	elif new_percentage >= 10:
		$Viewport/Control/Label.text = '0' + str(new_percentage) + '%'
	else:
		$Viewport/Control/Label.text = '00' + str(new_percentage) + '%'


func _on_new_percentage(new_percentages):
	set_percentage(round(new_percentages[key]) as int)
