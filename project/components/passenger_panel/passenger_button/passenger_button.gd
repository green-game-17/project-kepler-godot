extends StaticBody


signal switch_mode(key, mode)


export (String) var button_key
export (bool) var pressed_at_start = false

const ID = "passenger_button"
const MATERIAL_OFF = preload("res://assets/materials/button/button_off.tres")
const MATERIAL_ON = preload("res://assets/materials/button/button_on.tres")
const MOVE_SPEED = 1.5
const MOVE_DISTANCE = 0.7

enum MODE {OFF, ON}

var current_mode = MODE.OFF
var on_position
var off_position


func _ready():
	var current_rotation = get_rotation()
	var distance_vector = Vector3(0, -MOVE_DISTANCE * get_scale().y, 0)
	if current_rotation.x != 0:
		distance_vector = distance_vector.rotated(Vector3(1, 0, 0), current_rotation.x)
	if current_rotation.y != 0:
		distance_vector = distance_vector.rotated(Vector3(0, 1, 0), current_rotation.y)
	if current_rotation.z != 0:
		distance_vector = distance_vector.rotated(Vector3(0, 0, 1), current_rotation.z)
	off_position = get_translation()
	on_position = off_position + distance_vector
	
	if pressed_at_start:
		on_just_pressed()


func on_just_pressed():
	if current_mode == MODE.OFF:
		current_mode = MODE.ON
		$BodyMesh.set_surface_material(0, MATERIAL_ON)
	else:
		current_mode = MODE.OFF
		$BodyMesh.set_surface_material(0, MATERIAL_OFF)
	emit_signal("switch_mode", button_key, current_mode)


func _physics_process(delta):
	var dist = _get_distance(current_mode)
	if current_mode == MODE.ON:
		if dist > 0:
			translate_object_local(Vector3(0, -MOVE_SPEED * delta, 0))
			if dist - _get_distance(current_mode) < 0:
				set_translation(on_position)
	elif current_mode == MODE.OFF:
		if dist > 0:
			translate_object_local(Vector3(0, MOVE_SPEED * delta, 0))
			if dist - _get_distance(current_mode) < 0:
				set_translation(off_position)


func _get_distance(mode) -> float:
	if mode == MODE.ON:
		return get_translation().distance_to(on_position)
	else:
		return get_translation().distance_to(off_position)


func _on_send_dict(dict):
	$Viewport/Control/Label.set_text(dict[button_key].name)
