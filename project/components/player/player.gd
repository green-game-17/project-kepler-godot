extends KinematicBody


const MOVEMENT_SPEED = 10
const MOUSE_SPEED = 0.0025
const GRAVITY = 16
const MAX_VELOCITY = -50
const JUMP_STRENGTH = 10

var mouse_movement = Vector2(0, 0)
var selected_object
var velocity = 0
var on_floor = false
var current_camera = null
var start_pos


# Called when the node enters the scene tree for the first time.
func _ready():
	current_camera = $Camera
	start_pos = get_translation()


func _input(event):
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		# This needs to be done in _input because _ready is too early in the HTML5 export
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	if event is InputEventMouseMotion:
		mouse_movement -= event.get_relative() * MOUSE_SPEED
		mouse_movement.y = clamp(mouse_movement.y, -PI / 2, PI / 2)
		$Camera.transform.basis = Basis() # reset rotation
		$Camera.rotate_object_local(Vector3(0, 1, 0), mouse_movement.x) # first rotate in Y
		$Camera.rotate_object_local(Vector3(1, 0, 0), mouse_movement.y) # then rotate in X
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ESCAPE:
			get_tree().quit()


func _physics_process(delta):
	var player_movement = Vector3(0, 0, 0)
	if Input.is_action_pressed("ui_up"):
		player_movement.z = -1
	if Input.is_action_pressed("ui_down"):
		player_movement.z = 1
	if Input.is_action_pressed("ui_left"):
		player_movement.x = -1
	if Input.is_action_pressed("ui_right"):
		player_movement.x = 1
	
	player_movement = player_movement.rotated(Vector3(0, 1, 0), mouse_movement.x)
	player_movement = player_movement.normalized() * MOVEMENT_SPEED
	
	velocity -= GRAVITY * delta
	if velocity < MAX_VELOCITY:
		velocity = MAX_VELOCITY
	player_movement.y = velocity
	
	move_and_slide(player_movement, Vector3(0, 1, 0))
	
	if is_on_floor():
		if Input.is_action_just_pressed("jump"):
			velocity = JUMP_STRENGTH
		elif velocity < 0:
			velocity = 0
	if is_on_ceiling():
		velocity = 0
	
	if Input.is_action_just_pressed("ui_select"):
		selected_object = $Camera/RayCast.get_collider()
		if selected_object:
			print(selected_object.name)
		if selected_object is KinematicBody:
			selected_object.get_owner().on_press()
		if selected_object is StaticBody and "ID" in selected_object:
			if selected_object.ID == "button":
				selected_object.do_action()
			elif selected_object.ID == "passenger_button":
				selected_object.on_just_pressed()
	
	if Input.is_action_just_pressed("switch_camera"):
		current_camera.set_current(false)
		if current_camera == $Camera:
			current_camera = get_owner().get_node("Camera")
		else:
			current_camera = $Camera
		current_camera.set_current(true)
	
	if Input.is_action_just_pressed("restart"):
		set_translation(start_pos)
